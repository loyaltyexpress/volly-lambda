# volly-lambda

This is a repository to house all lambda related functions.

## Layers

We are using AWS Lambda Layers - which acts as a centralized way to share common code. Basically, the node_modules folder for those npm libraries.  If you navigate to the vollycore/vollycommon folder, you'll notice that there's no code - only a package.json and a node_modules inside a nodejs folder.  AWS will extract (and merge) this node_modules folder with other node_modules folders so that those libraries are available to your code with you needing to include it yourself.  You just need to require(xx) like normal.

The benefits of doing it this way is:
- it's easier to upgrade the lambda's if anything changes if an external library updates
- your lambda is a lot smaller - which is good in general

We are currently deploying everything as .zip files, so all functions/layers needs to be zipped and placed into the dist folder

 There's currently 3 layers (vollycore, vollycommon, vollysecurity)

### **Layer: vollycore**

This layer houses the @volly/core libraries, currently:

- @volly/config
- @volly/logging
- @volly/utils
- @volly/models
- dotenv

**NOTE: @volly/database wouldn't compile, so I skipped it for now**

### **Layer: vollycommon**

This layer houses libraries that are common to many applications

- lodash
- axios
- jsonwebtoken

### **Layer: vollysecurity**

Currently this own houses a volly cert (/opt/volly.pem), which is required to hit other endpoints on https

Here's some code on how you would use it

```
const url = "https://www.something.com";
        
const agent = new https.Agent({ca: fs.readFileSync('/opt/volly.pem'), requestCert: true });

const axiosConfig = {
  headers: {
    "someheader": "xx"
  },
  httpsAgent: agent
};
        
return axios.post(url, { "data": "data" }, axiosConfig);
```

**NOTE: If you need to change any of these layers, you'll need to work with Mitch to get them deployed.**

## Your Lambda Function

AWS Lambas are required to be compiled into js.  You can't deploy typescript (as far as I know).  So you can either write them in js, or write them in ts and compiled them to js.  The final js file needs to be zipped and placed into the the dist folder.

NOTE:  If your function needs some library that not included vollycore/vollycommon, then you will also need to include node_modules in your .zip file.











