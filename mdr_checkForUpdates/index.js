/* eslint-disable no-console */
const ConfigSDK = require("@volly/config");
const { default: axios } = require("axios");
const https = require("https");
const fs = require("fs");

exports.handler = async () => {
  const promise = new Promise((resolve, reject) => {
    const configSDK = new ConfigSDK.default();
    const configData = [{ name: "private_url", type: "parameter", appCode: "mgw" }];
    configSDK
      .get(configData)
      .then((config) => {
        const url = `${config.data.private_url}/los/schedule/checkForUpdates`;
        console.log(url);
        
        var agent = new https.Agent({ca: fs.readFileSync('/opt/volly.pem'), requestCert: true });

        const axiosConfig = {
          headers: {
            "volly-partner-id": "INT-LOS-8445"
          },
          httpsAgent: agent
        };
        
        return axios.post(url, {}, axiosConfig);
      })
      .then(() => {
        console.log("MDR.checkForUpdates called successfully");
        resolve({ statusCode: 204 });
      })
      .catch((err) => {
        console.log(err);
        reject(err);
      });
  });
  return promise;
};
