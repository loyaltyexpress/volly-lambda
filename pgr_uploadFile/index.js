/* eslint-disable no-console */
require("dotenv").config();
const ConfigSDK = require("@volly/config");
const { default: axios } = require("axios");
const jwt = require("jsonwebtoken");

exports.handler = async () => {
  console.log("pgr_uploadFile handler called");

  const promise = new Promise((resolve, reject) => {
    const configSDK = new ConfigSDK.default();
    const configData = [
      { name: "private_url", type: "parameter", appCode: "pgr", alias: "pgr_private_url" },
      { name: "auth_creds", type: "secret", appCode: "pgr" }
    ];

    let baseUrl = null;

    const axiosConfig = {
      headers: {
        "Content-Type": "application/json"
      }
    }

    console.log("Retrieving configuration");
    configSDK
      .get(configData)
      .then((config) => {
        baseUrl = config.data.pgr_private_url;
        const url = `${baseUrl}/auth`;

        const payload = {
          client_secret: config.data.auth_creds.client_secret,
        };

        const token = jwt.sign(payload, config.data.auth_creds.signature_key);

        axiosConfig.headers.Authorization = `Bearer ${token}`;
        console.log("Getting auth token");
        return axios.post(url, payload, axiosConfig);
      })
      .then((authResponse) => {
        const url = `${baseUrl}/nightly/upload`;
        const now = new Date();
        const dateStr = `${now.getMonth() + 1}/${now.getDate()}/${now.getFullYear()}`;

        const payload = {
          date: dateStr
        };

        axiosConfig.headers.Authorization = `Bearer ${authResponse.data.accesstoken}`;

        console.log("Sending file upload request to service");
        return axios.post(url, payload, axiosConfig);
      })
      .then(() => {
        console.log("PGR.uploadFile called successfully");
        resolve({ statusCode: 204 });
      })
      .catch((err) => {
        console.log(err.message);
        reject(err);
      });
  });
  return promise;
};
